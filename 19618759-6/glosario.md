﻿1. Control de versiones:   
   Asi es llamada la gestión de todos los distintos tipos de cambios de los elementos de un producto o algun tipo de configuracion, estos pueden ser una revision,version o edicion
   [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)	
2. Control de versiones distribuido:
   Es un tipo de control que permite tener como un "portal" donde se pueden compartir todo tipo de actualizaciones y ediciones de un trabajo en comun entre distintos desarrolladores.
   [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)	
3. Repositorio remoto y local:
   1. Repositorio remoto: Un repositorio remoto se define como una version del proyecto alojado en la nube del cual se esta trabajando
   2. Repositorio local: Un repositorio local se define como una version del proyecto alojado en el almacenamiento de tu equipo
   [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
4. Copia de trabajo/Working Copy:
   Una copia del proyecto en donde se podrá trabajar sin modificar el proyecto real que esta subido en la nube
   [Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)
5. Area de preparacion/Staging area:
   Es un archivo simple, generalmente contenido en el directorio del git y guarda informacion sobre que contendra el proximo guardado del proyecto.
   [Fuente](https://git-scm.com/book/en/v1/Getting-Started-Git-Basics)
6. Preparar cambios/ Stage changes:
   Es cuando se preparan los cambios para que los archivos esten listo para subirse al repositorio
   [Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
7. Confirmar cambios/Commit changes:
   Es cuando se guardan los cambios en el repositorio
   [Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
8. Commit:
   El commit sirve para guardar los cambios a tu repositorio local 
   [Fuente] (https://www.git-tower.com/learn/git/commands/git-commit)
9. Clone:
   Clona el repositorio que esta en la nube a un repositorio local del computador
   [Fuente] (https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone)
10. Pull: 
   Es un comando para obtener los archivos del repositorio de la nube, al repositorio local
   [Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
11. Push:
   Es un comando para guardar los archivos del repositorio local al repositorio de la nube
   [Fuente] (https://www.atlassian.com/git/tutorials/syncing/git-push)
12. Fetch:
   Comando que descarga el contenido del repositorio remoto especificado.
   [Fuente] (https://www.atlassian.com/git/tutorials/syncing/git-pull)
13. Merge:
   Es un comando que se invoca a traves de "git merge" en el bash, es un comando el cual combina multiples secuencias de coomits en un sola, su uso mas frecuente es para combinar 2 branches
   [Fuente] (https://www.atlassian.com/git/tutorials/using-branches/git-merge)
14. Status:
    El comando "git status", muestra el estado del directorio de trabajo y el area de preparacion, deja ver que cambios han sido preparados, cuales no y que archivos no estan siendo soportados por Git
    [Fuente] (https://www.atlassian.com/git/tutorials/inspecting-a-repository)
15. Log:
    Es un comando que muestra todos los commits hechos,ademas libera opciones aplicables para otro comando el cual es "git rev-list" para controlar que es mostrado y como es mostrado, ademas tambien libera opciones aplicables al comando "git diff-*" para controlar como los cambios de cada commit introducido son mostrados
    [Fuente] (https://git-scm.com/docs/git-log)
16. Checkout:
    Un comando que tiene distintas variaciones y aplicaciones, su principal funcion es para cambiar las Branches y recuperar archivos de trabajo del arbol
    [Fuente] (https://git-scm.com/docs/git-checkout)
17. Rama/Branch:
    Es simplemente un apuntador movil apuntando a un commit, por defecto se apunta a la rama master (branch master)
    [Fuente] (https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
18: Tags/Etiqueta:
    Las tags o etiquetas son referencias que apuntan a puntos especificos de un historial de proyecto en git, para poder indentificar segun las necesidades del usuario